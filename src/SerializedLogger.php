<?php

namespace Drupal\serialized_logger;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;

/**
 * Class SerializedLogger.
 *
 * Service that logs arbitrary data in the DB, in Json format.
 *
 * @package Drupal\serialized_logger
 */
class SerializedLogger {

  /**
   * The database table name.
   */
  const TABLE_NAME = 'serialized_logs';

  /**
   * A Connection object to interact with the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  public $database;

  /**
   * An instance of the Json Serializer, to store and read data.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  public $serializer;

  /**
   * SerializedLogger constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A Connection object to interact with the database.
   * @param \Drupal\Component\Serialization\Json $serializer
   *   An instance of the Json Serializer, to store and read data.
   */
  public function __construct(Connection $database, Json $serializer) {
    $this->database = $database;
    $this->serializer = $serializer;
  }

  /**
   * Write serialized data in the serialized_logs table.
   *
   * @param string $channel
   *   The name of the channel storing the data.
   * @param array $data
   *   The array of data to serialize and store.
   *
   * @throws \Exception
   */
  public function log($channel, array $data) {
    $serialized_data = $this->serializer->encode($data);

    $this->database->insert(self::TABLE_NAME)
      ->fields([
        'channel' => $channel,
        'data' => $serialized_data,
      ])
      ->execute();
  }

  /**
   * Returns all logs for a given channel, unserialized.
   *
   * @param string $channel
   *   The name of the channel from which you wish to retrieve the logs.
   *
   * @return array
   *   The logged data, unserialized.
   */
  public function getLogsByChannel($channel) {
    $records = $this->database->select(self::TABLE_NAME)
      ->fields(self::TABLE_NAME, ['lid', 'data'])
      ->condition('channel', $channel)
      ->execute()
      ->fetchAllKeyed();

    if (!empty($records)) {
      foreach ($records as $id => $record) {
        $records[$id] = $this->serializer->decode($record);
      }
    }

    return $records;
  }

}
